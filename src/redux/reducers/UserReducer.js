import {User} from '../../constants/ActionType'
import { updateObject } from '../../utilities/Utilities'

const initState = {
    loading : false,
    user : null
}

const loginStart=(state, action) => {
    console.log("--->", action);
    return updateObject(state, {
        loading : true,
        error : null
    })
}

const loginSuccess=(state, action) => {

}

const loginFail=(state, action) => {

}

export const UserReducer= (state = initState, action) => {
    switch (action.type) {
        case User.LOGIN_START:
            console.log("====>action", action);
            return loginStart(state,action)
        
        case User.LOGIN_SUCCESS:
            return loginSuccess(state,action);
            
        case User.LOGIN_FAIL:
            return loginFail(state,action);
            
        default:
            return state;
    }
}