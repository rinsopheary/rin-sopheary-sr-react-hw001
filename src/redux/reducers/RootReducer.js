import { combineReducers } from 'redux';
import { articleReducer } from './ArticleReducer';
import { CategoryReducer } from './CategoryReducer';
import { UserReducer } from './UserReducer';

export const rootReducer = combineReducers({
    user: UserReducer,
    article: articleReducer,
    category: CategoryReducer
})