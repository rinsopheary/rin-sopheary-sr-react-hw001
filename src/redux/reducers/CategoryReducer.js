import {
    updateObject
} from "../../utilities/Utilities";
import {
    Category
} from "../../constants/ActionType";

const initState = {
    loading: false,
    category: null,
    categoryList: []
}

//Fetch article
const fetchCategoryLoading = (state, action) => {
    return updateObject(state, {
        loading: true,

    })
}

const fetchCategoryLoadingSuccess = (state, action) => {
    return updateObject(state, {
        loading: false,
        categoryList: action.categories

    })
}

const fetchCategoryLoadingFail = (state, action) => {
    return updateObject(state, {
        loading: false,
        categoryList: []

    })
}

export const CategoryReducer = (state = initState, action) => {
    switch (action.type) {
        case Category.FETCH_LOADING_CATAGORY:
            return fetchCategoryLoading(state)
        case Category.FETCH_LOADING_SUCCESS_CATAGORY:
            return fetchCategoryLoadingSuccess(state, action)
        case Category.FETCH_LOADING_FAIL_CATAGORY:
            return fetchCategoryLoadingFail(state, action)
        default:
            return state;
    }

}