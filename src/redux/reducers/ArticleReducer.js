import {updateObject} from "../../utilities/Utilities";
import { Article } from "../../constants/ActionType";

const initState = {
    loading : false,
    article : null,
    articleList : []
}
//Fetch article

const fetchArticleLoading = (state, action) => {
    return updateObject(state, {
        loading : true,

    })
}

const fetchArticleLoadingSuccess = (state, action) => {
    return updateObject(state, {
        loading : false,
        articleList : action.articles

    })
}

const fetchArticleLoadingFail = (state, action) => {
    return updateObject(state, {
        loading : false,
        articleList : []

    })
}
const fetchArticleDetailLoading= (state,action) => {
    return updateObject(state, {
        loading : true
    })
}

const fetchArticleDetailLoadingSuccess= (state,action) => {
    return updateObject(state, {
        loading : false,
        articles : action.artiCleDetail
    })
}
export const articleReducer = (state=initState, action) => {
    switch (action.type) {
        case Article.FETCH_LOADING:
            return fetchArticleLoading(state)
        case Article.FETCH_LOADING_SUCCESS:
            return fetchArticleLoadingSuccess(state,action)
        case Article.FETCH_LOADING_FAIL:
            return fetchArticleLoadingFail(state,action)
            case Article.FETCH_DETAIL_LOADING:
                return fetchArticleDetailLoading(state,action)
                
        default:
            return state;
    }

}