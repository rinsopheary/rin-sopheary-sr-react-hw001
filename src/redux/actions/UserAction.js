import Axios from 'axios';
import {User} from '../../constants/ActionType';

const loginStart = () => {
    return {
        type : User.LOGIN_START
    }
}

const loginSuccess = (data) => {
    return {
        type : User.LOGIN_SUCCESS,
        data
    }
}

const loginFail = (error) => {
    return {
        type : User.LOGIN_FAIL,
        error
    }
}

export const loginAction = email => {
    return (dispatch) => {
        dispatch(loginStart());
    }
}