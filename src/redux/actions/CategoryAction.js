import Axios from "axios"
import {
    API
} from "../../constants/API"
import {
    Category
} from "../../constants/ActionType";


const fetchCategoryLoading = () => {
    return {
        type: Category.FETCH_LOADING_CATAGORY
    }
}

const fetchCategoryLoadingSuccess = (categories) => {
    return {
        type: Category.FETCH_LOADING_SUCCESS_CATAGORY,
        categories
    }
}

const fetchCategoryLoadingFail = (error) => {
    return {
        type: Category.FETCH_LOADING_FAIL_CATAGORY,
        error
    }
}

export const fetchCategoryList = () => {
    return (dispatch) => {
        dispatch(fetchCategoryLoading());
        Axios
            .get(`${API.baseURL}/categories/all`)
            .then((res) => dispatch(fetchCategoryLoadingSuccess(res.data)))
            .catch((err) => dispatch(fetchCategoryLoadingFail(err)))
        // .then((res) => console.log("-->rest", res))
        // .catch((err) => console.log("-->err", err))

    };
}