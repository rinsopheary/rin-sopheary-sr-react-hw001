import Axios from "axios"
import { API } from "../../constants/API"
import { Article } from "../../constants/ActionType";


const fetchArticleLoading = () => {
    return {
        type : Article.FETCH_LOADING
    }
}

const fetchArticleLoadingSuccess = (articles) => {
    return {
        type : Article.FETCH_LOADING_SUCCESS,
        articles
    }
}

const fetchArticleLoadingFail = (error) => {
    return {
        type : Article.FETCH_LOADING_FAIL,
        error
    }
}

export const fetchArticleList = () => {
    return(dispatch) => {
        dispatch(fetchArticleLoading());
        Axios
            .get(`${API.baseURL}/articles/`)
            .then((res) => dispatch(fetchArticleLoadingSuccess(res.data)))
            .catch((err) => dispatch(fetchArticleLoadingFail(err)))
            // .then((res) => console.log("-->rest", res))
            // .catch((err) => console.log("-->err", err))
            
    };
}

//article detail

const fetchAricleDetailLoading= () => ({
    type: Article.FETCH_DETAIL_LOADING
})

const fetchArticleDetailLoadingSuccess = (articleDetail) => ({
    type: Article.FETCH_DETAIL_LOADING_SUCCESS,
    articleDetail
})

const fetchArticleDetailLoadingFaile = (error) => ({
    type: Article.FETCH_DETAIL_LOADING_FAIL,
    error
})

export const fectchArticleDetail = articleId => {
    return dispatch => {
        dispatch(fetchArticleLoading());
        Axios
            .get(`${API.baseURL}/articles/${articleId}`)
            .then((res) => dispatch(fetchArticleDetailLoadingSuccess(res.data)))
            .catch((err) => dispatch(fetchArticleDetailLoadingSuccess(err)))
            // .then((res) => console.log("-->rest", res))
            // .catch((err) => console.log("-->err", err))
            
    }
}

//fetching 