import React, { Component } from 'react'

export default class Error404Screen extends Component {
    render() {
        return (
            <div className="container">
                <h1>Error 404 Not found</h1>
            </div>
        )
    }
}
