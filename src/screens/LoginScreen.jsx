import React, { Component } from 'react';
import {Form, Button, Card} from 'react-bootstrap';
import { connect } from 'react-redux';
import  '../screens/styles/login-screen-style.css';
import {loginAction} from '../redux/actions/UserAction'

class LoginScreen extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
             email : ""
        }
    }

    handleChange = e =>{
        this.setState({
            [e.target.name] : e.target.value
        });
    }

    handleLogin = e => {
        e.preventDefault();
        const {email} = this.state
        this.props.onLogin(email)
    }
    
    render() {
        const {email} = this.state.email;
        console.log("====>", this.props.loading);
        return (
                <div className="my-card">
                    <h2 className="text-center">Welcome to ReactNative Course</h2> <br/>
                <Card className="pt-5 pb-5 pl-5 pr-5">
                    <Form onSubmit={this.handleLogin}>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Control 
                            type="email" 
                            placeholder="Enter email"
                            value = {email}
                            name = "email"
                            onChange = {this.handleChange}
                            />
                            {/* <Form.Control type="button" variant="primary" value="Login" /> */}
                            <br/>
                            <button className="btn btn-primary" style={{width: "100%"}}>Login</button>
                        </Form.Group>  
                    </Form>
                </Card>
                <br/>
                <h5>2020 &#169; ReactNative Course. All rights reserved.</h5>
                </div>
            
        )
    }
}

const mapStateToProps= (state) => {
    return {
        loading : state.user.loading
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onLogin: (email) => dispatch(loginAction(email))
    };
}
export default connect(mapStateToProps,  mapDispatchToProps) (LoginScreen)
