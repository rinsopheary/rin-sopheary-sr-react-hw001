import React, { Component } from 'react';
import { Tab, Row, Col, ListGroup, NavLink, Card, Image, Button } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { connect } from 'react-redux';
import { fetchArticleList } from '../redux/actions/ArticleAction';
import { fetchCategoryList } from '../redux/actions/CategoryAction';
import { Link } from 'react-router-dom';

class HomeScreen extends Component {

  componentDidMount() {
    this.props.fetchArticleList()
    this.props.fetchCategoryList()

  }

  render() {
    console.log(this.props.articleList);
    console.log(this.props.categoryList);

    const categoryData = this.props.categoryList.map(t =>
      <ListGroup>
        <ListGroup.Item action>{t.categoryName}</ListGroup.Item>
      </ListGroup>)

    const articleData = this.props.articleList.map(t =>
      <Tab.Content>
        <Tab.Pane eventKey="#link1">
          <Card border="dark">
            <Card.Header>
              <Image src={t.user.profile} roundedCircle width={50} height={50} /> {t.user.userName}
              <div className="div text-right">
                <Button variant="primary" size="sm" width={40}>
                  {t.category.categoryName}
                </Button>{' '} | {t.createdDate}
              </div>
            </Card.Header>
            <Card.Body>
              <Card.Title>{t.title}</Card.Title>
              <Card.Img variant="top" src={t.imageUrl} />
              <Link to={`/article/${t.articleId}`}>See more</Link>

            </Card.Body>
          </Card>
          <br />
        </Tab.Pane>
      </Tab.Content>

    )
    return (
      <div className="container">
        <Tab.Container id="list-group-tabs-example" defaultActiveKey="#link1">
          <Row className="pt-5">
            <div>
              <NavLink><h5>បន្ថែមអត្ថបទថ្មី</h5> </NavLink>
            </div>
          </Row>
          <Row className="pt-3">
            <Col sm={4}>

              {/* <ListGroup>
        <ListGroup.Item action href="#link1">
          អត្ថបទគ្រប់ប្រភេទ
        </ListGroup.Item>
        <ListGroup.Item action href="#link2">
          {categoryData}
        </ListGroup.Item>
      </ListGroup> */}
              {categoryData}
            </Col>
            <Col sm={8}>
              {/* <Tab.Content>
        <Tab.Pane eventKey="#link1">Hello</Tab.Pane>
      </Tab.Content>

      <Tab.Content>
        <Tab.Pane eventKey="#link2"> how are you</Tab.Pane>
      </Tab.Content> */}
              {articleData}
            </Col>
          </Row>
        </Tab.Container>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  articleList: state.article.articleList,
  categoryList: state.category.categoryList,
  loading: state.category.loading,
  loading: state.article.loading
})

const mapDispatchToProps = (dispatch) => ({
  fetchArticleList: () => dispatch(fetchArticleList()),
  fetchCategoryList: () => dispatch(fetchCategoryList())
});

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen)
