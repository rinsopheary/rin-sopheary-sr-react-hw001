import React, { Component } from 'react'
import { connect } from 'react-redux';
import {fectchArticleDetail, fetchArticleDetail} from "../redux/actions/ArticleAction"

class ArticleDetailScreen extends Component {

    componentDidMount(){
        const id = this.props.match.params.articleId;
        console.log("===>id=", id);
        this.props.fetchArticleDetail(id)
    }
    render() {
        const detailData = this.props
        return (
            <div className="container">
                <h1>Detail Screen</h1>
            </div>
        )
    }
}

const mapStateToProps = state => ({

})

const mapDispatchToProp = dispatch => ({
    fetchArticleDetail : (articleId) => dispatch(fectchArticleDetail(articleId))
})

export default connect(mapStateToProps, mapDispatchToProp)(ArticleDetailScreen)


