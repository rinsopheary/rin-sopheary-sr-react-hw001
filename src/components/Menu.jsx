import React from 'react';
import {Navbar,Nav,NavDropdown,Form,FormControl,Button} from 'react-bootstrap';
import {Link, NavLink} from 'react-router-dom';

export default function Menu() {
    return (
        <div>
            <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                <div className="container">
                <Nav.Link as={Link} to="/">
                    <Navbar.Brand>អត្ថបទព័ត៌មាន</Navbar.Brand>
                </Nav.Link>
            
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
                <Nav className="mr-auto"></Nav>
                <Nav>
                <Nav.Link as={Link} to="/user">អ្នកប្រើប្រាស់</Nav.Link>
                <Nav.Link as={Link} to="/article-type">ប្រភេទអត្ថបទ</Nav.Link>
                <Nav.Link as={Link} to="/article">អត្ថបទ</Nav.Link>
                <Nav.Link as={Link} to="/login">Login</Nav.Link>
                {/* <NavDropdown title="Admin" id="collasible-nav-dropdown">
                    <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
                    <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
                    <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
                </NavDropdown> */}
                </Nav>
            </Navbar.Collapse>
            </div>
            </Navbar>
        </div>
    )
}
