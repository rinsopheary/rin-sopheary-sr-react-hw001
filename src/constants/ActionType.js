export const User = {
    LOGIN_START : "USER_LOGIN_START",
    LOGIN_SUCCESS : "LOGIN_SUCCESS",
    LOGIN_FAIL : "LOGIN_FAIL"
}

export const Article = {
    FETCH_LOADING : "ARTICLE_FETCH_LOADING",
    FETCH_LOADING_SUCCESS : "ARTICLE_FETCH_LOADING_SUCCESS",
    FETCH_LOADING_FAIL : "ARTICLE_FETCH_LOADING_FAIL",

    FETCH_DETAIL_LOADING: "ARTICLE_FETCH_DETAIL_LOADING",
    FETCH_DETAIL_LOADING_SUCCESS: "ARTICLE_FETCH_DETAIL_LOADING_SUCCESS",
    FETCH_DETAIL_LOADING_FAIL: "ARTICLE_FETCH_DETAIL_LOADING_FAIL",
}

export const Category = {
    FETCH_LOADING_CATAGORY : "CATAGORY_FETCH_LOADING",
    FETCH_LOADING_SUCCESS_CATAGORY : "CATAGORY_FETCH_LOADING_SUCCESS",
    FETCH_LOADING_FAIL_CATAGORY : "CATAGORY_FETCH_LOADING_FAIL",
}