import React from 'react';
import logo from './logo.svg';
import './App.css';
import Menu from './components/Menu'
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import HomeScreen from './screens/HomeScreen'
import Error404Screen from './screens/Error404Screen';
import LoginScreen from './screens/LoginScreen';
import ArticleScreen from './screens/ArticleScreen';
import ArticleDetailScreen from './screens/ArticleDetailScreen';

function App() {
  return (
    <BrowserRouter>
    <Menu />
      <Switch>
        <Route path="/" exact component={HomeScreen}></Route>
        <Route path="/article/:articleId" component={ArticleDetailScreen}/>
        <Route path="/login" component={LoginScreen}></Route>
        <Route path="*" component={Error404Screen}></Route>
      </Switch>
    </BrowserRouter>
  );
}

export default App;
